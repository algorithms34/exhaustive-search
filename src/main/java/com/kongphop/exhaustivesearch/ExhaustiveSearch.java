/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.kongphop.exhaustivesearch;

/**
 *
 * @author Admin
 */
public class ExhaustiveSearch {

    static int exhSearch(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                return i;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int[] x = {9, 2, 7, 6, 4, 3, 5, 8, 1};

        System.out.println(exhSearch(x, 1));
        System.out.println(exhSearch(x, 2));
        System.out.println(exhSearch(x, 3));
    }

}
